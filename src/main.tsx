import ReactDOM from "react-dom/client";
import jss from "jss";
import preset from "jss-preset-default";
import App from "./App.tsx";

jss.setup(preset());

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <App />
);
