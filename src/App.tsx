import Select from "@/components/atoms/Select/Select";
import { useState } from "react";
import { createUseStyles } from "react-jss";

const useStyles = createUseStyles({
  "@global": {
    "*": {
      boxSizing: "border-box",
    },
    body: {
      margin: 0,
      padding: 0,
      fontFamily: "sans-serif",
    },
  },
  main: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
    flexDirection: "column",
    gap: 32,
  },
  hint: {
    color: "gray",
  },
});

function App() {
  const classes = useStyles();
  const [selected, setSelected] = useState("");

  return (
    <main className={classes.main}>
      <Select
        items={["Education", "Science", "Art", "Sport", "Games", "Health"]}
        onChange={setSelected}
      />

      <p className={classes.hint}>
        {selected
          ? `Selected item is: ${selected}`
          : "Please select or add an item."}
      </p>
    </main>
  );
}

export default App;
