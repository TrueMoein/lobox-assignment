import { useEffect } from "react";

export const useOutsideClick = (callback: () => void, element: HTMLElement) => {
  useEffect(() => {
    function handleClick(e: globalThis.MouseEvent) {
      if (!element) return;
      if (!element.contains(e?.target as HTMLElement)) {
        callback();
      }
    }

    document.addEventListener("click", handleClick);
    return () => {
      document.removeEventListener("click", handleClick);
    };
  }, [callback, element]);
};
