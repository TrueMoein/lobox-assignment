import { ChangeEvent, KeyboardEvent, useEffect, useRef, useState } from "react";
import { useStyles } from "./Select.styles";
import cs from "classnames";
import { useOutsideClick } from "@/hooks/useOutsideClick";

type SelectProps = {
  items: string[];
  onChange?: (selected: string) => void;
};

export default function Select({ items: defaultItems, onChange }: SelectProps) {
  const SelectContainerRef = useRef<HTMLDivElement>(null);
  const [items, setItems] = useState(defaultItems);
  const [inputValue, setInputValue] = useState("");
  const [selected, setSelected] = useState("");
  const [isOpen, setIsOpen] = useState(false);
  const classes = useStyles({ isOpen, hasItem: !!items.length });
  useOutsideClick(
    () => setIsOpen(false),
    SelectContainerRef.current as HTMLElement
  );

  useEffect(() => {
    if (onChange) {
      onChange(selected);
    }
  }, [selected, onChange]);

  function handleKeyUp(e: KeyboardEvent<HTMLInputElement>) {
    if (e.key == "Enter" && inputValue) {
      if (!items.includes(inputValue)) {
        setItems([inputValue, ...items]);
        setIsOpen(true);
      } else {
        setIsOpen(false);
      }
      setSelected(inputValue);
    } else if (["ArrowUp", "ArrowDown"].includes(e.key)) {
      setIsOpen(true);
      const selectedIndex = items.indexOf(selected);
      const arrow =
        e.key == "ArrowUp"
          ? selectedIndex <= 0
            ? items[items.length - 1]
            : items[selectedIndex - 1]
          : selectedIndex >= items.length - 1
          ? items[0]
          : items[selectedIndex + 1];

      setSelected(arrow);
      setInputValue(arrow);
    }
  }

  const handleSelectItem = (item: string) => () => {
    setSelected(item);
    setInputValue(item);
    setIsOpen(false);
  };

  function handleChangeInput(e: ChangeEvent<HTMLInputElement>) {
    setInputValue(e.target.value);
    setIsOpen(false);
  }

  function handleOpenDropDown() {
    setIsOpen(true);
    if (selected !== inputValue) {
      setInputValue(selected);
    }
  }

  return (
    <div className={classes.dropDownWrapper} ref={SelectContainerRef}>
      <input
        className={classes.input}
        onClick={handleOpenDropDown}
        onFocus={handleOpenDropDown}
        value={inputValue}
        onChange={handleChangeInput}
        onKeyUp={handleKeyUp}
        placeholder={`${items.length ? "Select" : "Add"} an item...`}
      />
      {!!items.length && (
        <div
          className={cs([
            classes.dropDown,
            { [classes.hiddenDropDown]: !isOpen },
          ])}
        >
          <ul>
            {items.map((item) => (
              <li
                key={item}
                onClick={handleSelectItem(item)}
                {...(item == inputValue && { className: classes.selectedItem })}
              >
                {item}
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}
