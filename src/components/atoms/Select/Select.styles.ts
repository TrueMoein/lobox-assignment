import { createUseStyles } from "react-jss";

type RuleNames =
  | "dropDownWrapper"
  | "input"
  | "dropDown"
  | "selectedItem"
  | "hiddenDropDown";

export const useStyles = createUseStyles<
  RuleNames,
  { isOpen: boolean; hasItem: boolean }
>({
  dropDownWrapper: (props) => ({
    position: "relative",
    ...(props.hasItem && {
      "&::after": {
        content: '"⌄"',
        position: "absolute",
        right: 10,
        fontSize: 24,
        top: props.isOpen ? 38 : 26,
        color: "gray",
        transform: `scale(${props.isOpen ? -1 : 1})`,
        lineHeight: 0,
        transition: "300ms",
      },
    }),
  }),
  input: {
    width: "100%",
    borderRadius: 16,
    outline: "none",
    border: "2px #888888 solid",
    fontSize: 24,
    padding: 16,
    paddingRight: 24,
    "&:focus": {
      border: "2px rgb(0 135 243) solid",
      outline: "4px #D8E1FC solid",
    },
  },
  dropDown: {
    width: "100%",
    border: "1px solid #dedede",
    position: "absolute",
    marginTop: 12,
    fontSize: 18,
    borderRadius: 16,
    padding: 8,
    maxHeight: 320,
    overflow: "scroll",
    transition: "200ms",
    backgroundColor: "white",

    "& ul": {
      listStyle: "none",
      padding: 0,
      display: "flex",
      flexDirection: "column",
      gap: 2,

      "& li": {
        cursor: "pointer",
        transition: "150ms",
        borderRadius: 8,
        padding: [12, 16],
        wordBreak: "break-word",
        "&:hover": {
          backgroundColor: "#2196f312",
        },
      },
    },
  },
  hiddenDropDown: {
    opacity: 0,
    pointerEvents: "none",
    marginTop: "-10px!important",
    scale: "0.9",
  },
  selectedItem: {
    backgroundColor: "#2196f31a",
    position: "relative",
    fontWeight: 400,
    color: "rgb(0 135 243)",
    "&::after": {
      content: '"✔"',
      position: "absolute",
      right: 16,
    },
  },
});
